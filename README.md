# Real Time Web 

In this repository you can find different ways to get real time (or near real time) updates in the browser.

I demonstrate the following mechanisms:

* Polling
* Long Polling
* Server Sent Events
* Websockets 
* SignalR 

For the last case, SignalR, I also show how to create a DotNet Console Client.

Projects in this repository

* [Web Application](WebApp)

* [Console Client](ConsoleClient)
